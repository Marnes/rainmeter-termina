[Variables]
; either black, color, or white
Theme=color
; You could make a custom theme by adding a folder with the appropriate image files in @Resources.
; You'd simply need to put the folder name here and use the existing images as reference for your own theme.

; performance scale: 1 = 100% (~60fps), 2 = 50%, 3 = 33%, 4 = 25%, etc
; fractional numbers (e.g. 1.5) not supported by Rainmeter, sorry
FrameRateDivider=1
; CPU usage is roughly linear to the target framerate, which is approx. 60fps divided by this value. Setting this too high will make the visual effects look weird.
