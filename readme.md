# Termina

A Rainmeter clock based on The Legend of Zelda: Majora's Mask and other fan art based on the same.

Developed on 3rd party request for personal use. Not intended for commercial purposes, only for online publication aimed at free (gratis and libre) personal use by all.

## Rainmeter users

Edit settings.inc next to this readme to choose a theme. Refresh the skin or restart Rainmeter to apply your changes.

## History and clock face authors

Previous version and more info published on DeviantArt: [Termina 3.1 on DA](https://www.deviantart.com/faziri/art/Termina-3-1-493503955)

### `color` theme

Unfortunately, the original publication of the colored clock face images by mntorankusu has disappeared: [Clock Tower face on DA](https://mntorankusu.deviantart.com/art/Majora-s-Mask-Clock-Town-clock-tower-face-415912981). Fortunately, Google and TinEye know of some lingering copies on obscure websites.

### `black`/`white` themes

The original black & white themes can still be found: [Termina Clock 1.01](https://web.archive.org/web/20180208235752/https://customize.org/rainmeter/skins/68862). According to that page, Bacushi permits derived works.

I believe the black/white themes used now are original work by myself, based on (traced over, probably) the color theme. I have no recollection where else they could have come from.
