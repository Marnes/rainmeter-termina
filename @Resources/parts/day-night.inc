; 0–23
[MeasureIntegerHourOfDay24]
Measure=Calc
Formula=Trunc(MeasureFractionalSeconds / 3600 % 24)

; 24h day: 6h night 12h day, 6h night
; we want a boolean indicating day (6:00 onward) or night (18:00 onward)
; solution: shift noon to 0 and check bounds at -6/+6
;
; this solution is much more intuitive when graphed
; truncation/ticking of hours ensures a transition only occurs at hh:00:00.000
; 11.5 could be any number in [11, 12[
; absolute value allows us to check just 1 boundary instead of 2
[MeasureHalfOfDay]
Measure=Calc
Formula=Abs(MeasureIntegerHourOfDay24 - 11.5) < 6 ? 1 : 0
; less cryptical for filenames than 0/1
Substitute="0": "night", "1": "day"

[HourFileName]
Measure=String
String=#@#themes\#Theme#\hours-[MeasureHalfOfDay].png
DynamicVariables=1
