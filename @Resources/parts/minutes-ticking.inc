[Variables]
StartJoltingMinuteAtSeconds=58
MinuteJolts=2
MinuteJoltScale=0.3
StopJoltingMinuteAtSeconds=58.5
RollOverMinuteAtSeconds=59

[MinuteJoltDuration]
Measure=Calc
Formula=1 / #MinuteJolts#
UpdateDivider=-1

; 0—1 progress through jolt time window
[MeasureMinuteJoltProgress]
Measure=Calc
Formula=(MeasureFractionalSecondsOfMinute >= #StartJoltingMinuteAtSeconds#) && (MeasureFractionalSecondsOfMinute < #StopJoltingMinuteAtSeconds#) ? ((MeasureFractionalSecondsOfMinute - #StartJoltingMinuteAtSeconds#) / (#StopJoltingMinuteAtSeconds# - #StartJoltingMinuteAtSeconds#)) : 0

; pre-tick jolting
[MeasureMinuteJolt]
Measure=Calc
Formula=MeasureMinuteJoltProgress > 0 ? (Min(MeasureMinuteJoltProgress % MinuteJoltDuration, MinuteJoltDuration - (MeasureMinuteJoltProgress % MinuteJoltDuration)) * #MinuteJoltScale#) : 0

; tick
[MeasureMinuteRollOver]
Measure=Calc
Formula=MeasureFractionalSecondsOfMinute >= #RollOverMinuteAtSeconds# ? (((MeasureFractionalSecondsOfMinute - #RollOverMinuteAtSeconds#) / (60 - #RollOverMinuteAtSeconds#)) ** 2) : 0

; 0–59
[MeasureIntegerMinutesOfHour]
Measure=Calc
Formula=Trunc(MeasureFractionalSeconds / 60 % 60)

[MeasureSpringingMinutesOfHour]
Measure=Calc
Formula=MeasureIntegerMinutesOfHour + MeasureMinuteJolt + MeasureMinuteRollOver
MinValue=0
MaxValue=60

[MeterMinutes]
MeasureName=MeasureSpringingMinutesOfHour
Meter=Rotator
ImageName=#@#themes\#Theme#\minutes.png
OffsetX=125
OffsetY=125
X=150
Y=150
