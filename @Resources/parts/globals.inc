; refresh ~60hz
; size 300x300px
[Rainmeter]
Update=16
DefaultUpdateDivider=#FrameRateDivider#
SkinWidth=300
SkinHeight=300

; linear/infinite integer seconds
; s, s+1, s+2, ...
[MeasureNow]
Measure=Time

[FramesPerSecond]
Measure=Calc
Formula=1000 / 16 / #FrameRateDivider#
UpdateDivider=-1

; average over 1s to emulate fraction of second resolution
; s, s.1, s.2, ... s.9, s+1, s+1.1, ...
[MeasureFractionalSeconds]
Measure=Calc
; get numeric seconds, not freaky dual string/number stuff
; 1.5s is the effective offset from realtime, somehow
Formula=[MeasureNow:Timestamp] + 1.5
AverageSize=[FramesPerSecond]
DynamicVariables=1

; [0.0, 60.0[
[MeasureFractionalSecondsOfMinute]
Measure=Calc
Formula=MeasureFractionalSeconds % 60
