[Variables]
StartJoltingHourAtSeconds=3597
HourJolts=2
HourJoltScale=0.25
StopJoltingHourAtSeconds=3597.5
RollOverHourAtSeconds=3598

[HourJoltDuration]
Measure=Calc
Formula=1 / #HourJolts#
UpdateDivider=-1

; [0.0, 3600.0[
[MeasureFractionalSecondsOfHour]
Measure=Calc
Formula=MeasureFractionalSeconds % 3600

; 0—1 progress through jolt time window
[MeasureHourJoltProgress]
Measure=Calc
Formula=(MeasureFractionalSecondsOfHour >= #StartJoltingHourAtSeconds#) && (MeasureFractionalSecondsOfHour < #StopJoltingHourAtSeconds#) ? ((MeasureFractionalSecondsOfHour - #StartJoltingHourAtSeconds#) / (#StopJoltingHourAtSeconds# - #StartJoltingHourAtSeconds#)) : 0

; pre-tick jolting
[MeasureHourJolt]
Measure=Calc
Formula=MeasureHourJoltProgress > 0 ? (Min(MeasureHourJoltProgress % HourJoltDuration, HourJoltDuration - (MeasureHourJoltProgress % HourJoltDuration)) * #HourJoltScale#) : 0

; tick
[MeasureHourRollOver]
Measure=Calc
Formula=MeasureFractionalSecondsOfHour >= #RollOverHourAtSeconds# ? (((MeasureFractionalSecondsOfHour - #RollOverHourAtSeconds#) / (3600 - #RollOverHourAtSeconds#)) ** 2) : 0

; 0–11
[MeasureIntegerHour12]
Measure=Calc
Formula=Trunc(MeasureFractionalSeconds / 3600 % 12)

[MeasureSpringingHour12]
Measure=Calc
Formula=MeasureIntegerHour12 + MeasureHourJolt + MeasureHourRollover
MinValue=0
MaxValue=12

[MeterHours]
@Include=#@#parts\day-night.inc
MeasureName=MeasureSpringingHour12
Meter=Rotator
ImageName=[HourFileName]
OffsetX=96
OffsetY=96
X=150
Y=150
DynamicVariables=1
