; exponentiate the fraction to go 0.0–1.0, 1.0–2.0, etc with acceleration instead of linearly
[MeasureSpringingSecondsOfMinute]
Measure=Calc
Formula=Trunc(MeasureFractionalSecondsOfMinute) + Frac(MeasureFractionalSecondsOfMinute) ** 5
MinValue=0
MaxValue=60

[MeterSeconds]
MeasureName=MeasureSpringingSecondsOfMinute
Meter=Rotator
ImageName=#@#themes\#Theme#\seconds.png
OffsetX=10
OffsetY=110
X=150
Y=150
